<?php

namespace AssetManifest;

class AssetManifest {

	protected $manifest = [];

	public function __construct(){
		$manifest_file = __DIR__ . '/../data/assets_manifest.php';
		if(file_exists($manifest_file)){
			$this->manifest = require $manifest_file;
		}
	}

	public function lookup($source){
		if(isset($this->manifest[$source])){
			return $this->manifest[$source];
		}
		return $source;
	}

}
